/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////
//WARNING
//This file has been autogenerated.
//Do NOT make modifications directly to it as they will be overwritten!
/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////

#ifndef EXAMPLESHOPPINGCART_API_H
#define EXAMPLESHOPPINGCART_API_H

#include <CoreAPI.h>

/**
 API Version:       ExampleShoppingCart API v1

 New Functions:
 AddItem - 
 */

/**
  @brief AddItem v1
         Adds an item to the shopping cart.

  @param item_name
         The name of the item.
  @param price
         The price of the item.

  @return APIResult
          The result of the function. See CoreAPI.h for information on each result.
  */

typedef APIResult (GEARS_API *ExampleShoppingCart_AddItem_Func_v1)(_In_ const char* item_name, _In_ float32_t price);

typedef struct ExampleShoppingCartAPI_v1
{
  ExampleShoppingCart_AddItem_Func_v1   AddItem;
} ExampleShoppingCartAPI_v1;

static const char* ExampleShoppingCartAPI_Handle = "ExampleShoppingCartAPI";

#endif
